from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.forms import ModelForm
from woocommerce import API
from decimal import Decimal
import urllib2
import json

# Create your models here.

class ShippingAddress(models.Model):
    user = models.ForeignKey(User)
    address = models.TextField(max_length = 2048, blank = True, unique = False)
    postcode = models.CharField(max_length = 10, blank = True, unique = False)

    class Meta:
        verbose_name = "Shipping Address"
        verbose_name_plural = "Shipping Addresses"

    def __unicode__(self):
        return self.user.username + ": " + self.postcode
        



class Book(models.Model):
    
    GENRE_CHOICES = (
        ('ANT', 'Antiquarian & collectable'),
        ('ART', 'Art & photography'),
        ('AUDIO', 'Audio books'),
        ('BIOG', 'Biography'),
        ('BUS', 'Business, finance & law'),
        ('CHILD', 'Children\'s books'),
        ('CRIME', 'Crime, thrillers & mystery'),
        ('FICT', 'Fiction'),
        ('FOOD', 'Food & drink'),
        ('HEALTH', 'Health, family & lifestyle'),
        ('HIST', 'History'),
        ('HOME', 'Home & garden'),
        ('HUM', 'Humour'),
        ('LANG', 'Languages'),
        ('MIND', 'Mind, body & spirit'),
        ('MUSIC', 'Music, stage & screen'),
        ('POET', 'Poetry, drama & criticism'),
        ('REF', 'Reference'),
        ('REL', 'Religion & spirituality'),
        ('SCI', 'Science & nature'),
        ('SCIFI', 'Science fiction & fantasy'),
        ('SOC', 'Society & philosophy'),
        ('SPORT', 'Sports, hobbies & games'),
        ('TECT', 'Technology'),
        ('TRAVEL', 'Travel & holiday')
    )
    
    isbn = models.CharField(max_length = 13, blank = True, unique = True)
#    rawJSON = models.CharField(max_length = 65536, blank = True)
    title = models.CharField(max_length = 256, blank = True)
    publisher = models.CharField(max_length = 256, blank = True)
    author = models.CharField(max_length = 256, blank = True)
    genre = models.CharField(max_length = 8, blank = True, choices = GENRE_CHOICES)
    summary = models.TextField(max_length = 65536, blank = True)
    
    def save(self, *args, **kw):
        try:
            response = urllib2.urlopen('http://isbndb.com/api/v2/json/VPOP2GOU/book/' + self.isbn)
            rawJSON = response.read()
            data = json.loads(rawJSON)
        except:
            pass
        try:
            self.author = data['data'][0]['author_data'][0]['name']
        except:
            pass
        try:
            self.title = data['data'][0]['title']
        except:
            pass
        try:
            self.publisher = data['data'][0]['publisher_name']
        except:
            pass
        try:
            self.summary = data['data'][0]['summary']
        except:
            pass
            
        super(Book, self).save(*args, **kw)
        
    def __unicode__(self):
        return self.isbn + ' (' + self.title + ')'  
        
        
class Product(models.Model):
    
    CONDITION_CHOICES = (
        ('LN', 'Like New'),
        ('VG', 'Very Good'),
        ('G', 'Good'),
        ('A', 'Acceptable')
    )
    
    book = models.ForeignKey(Book)
    user = models.ForeignKey(User)
    price = models.DecimalField(max_digits = 5, decimal_places = 2, default = 0.00)
    condition = models.CharField(max_length = 4, blank = True, choices = CONDITION_CHOICES, default = 'G')
    forsale = models.BooleanField(default=True)

    def book_title(self):
            return book.title
    
    def book_isbn(self):
            return book.isbn
    
    def book_genre(self):
            return book.genre
    
    def woocommerce_id(self):
        return str(self.user.id) + "_" + self.book.isbn
    
    def __unicode__(self):
        return self.user.username + ": " + self.book.isbn + ' (' + self.book.title + ')'
        
    def save(self, *args, **kw):
        wcapi = API(
            url="http://www.zappy.click",
            consumer_key    = settings.WOOCOMMERCE_API_KEY,
            consumer_secret = settings.WOOCOMMERCE_API_SEC,
        )
        
        data = {
            "product": {
                "title": self.book.title,
                "type": "simple",
                "regular_price": str(self.price),
                "description": self.book.summary,
                "short_description": self.book.summary,
            }
        }

        wc_response = wcapi.post("products", data)
        super(Product, self).save(*args, **kw)
        
class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ['book_title', 'book_isbn', 'book_genre', 'price', 'condition', 'forsale']




              