# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zapsell', '0003_auto_20150919_0713'),
    ]

    operations = [
        migrations.RenameField(
            model_name='book',
            old_name='isbn10',
            new_name='isbn',
        ),
        migrations.RemoveField(
            model_name='book',
            name='isbn13',
        ),
    ]
