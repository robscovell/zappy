# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zapsell', '0008_seller_shippingaddress'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='shippingaddress',
            options={'verbose_name': 'Shipping Addresse'},
        ),
        migrations.AlterField(
            model_name='shippingaddress',
            name='address',
            field=models.TextField(max_length=2048, blank=True),
        ),
    ]
