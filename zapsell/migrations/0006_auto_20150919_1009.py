# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zapsell', '0005_auto_20150919_0847'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='rawJSON',
        ),
        migrations.AlterField(
            model_name='book',
            name='isbn',
            field=models.CharField(unique=True, max_length=13, blank=True),
        ),
    ]
