# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('isbn13', models.CharField(max_length=10)),
                ('isbn10', models.CharField(max_length=13)),
                ('rawJSON', models.CharField(max_length=2048)),
                ('title', models.CharField(max_length=256)),
                ('publisher_text', models.CharField(max_length=256)),
                ('author', models.CharField(max_length=256)),
                ('summary', models.CharField(max_length=2048)),
            ],
        ),
    ]
