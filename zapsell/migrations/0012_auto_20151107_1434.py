# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zapsell', '0011_auto_20151107_1430'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shippingaddress',
            old_name='user',
            new_name='seller',
        ),
        migrations.AlterField(
            model_name='product',
            name='user',
            field=models.ForeignKey(to='zapsell.Seller'),
        ),
    ]
