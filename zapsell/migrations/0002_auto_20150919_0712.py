# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zapsell', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='author',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='isbn10',
            field=models.CharField(max_length=13, blank=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='isbn13',
            field=models.CharField(max_length=10, blank=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='publisher_text',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='rawJSON',
            field=models.CharField(max_length=2048, blank=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='summary',
            field=models.CharField(max_length=65536, blank=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='title',
            field=models.CharField(max_length=256, blank=True),
        ),
    ]
