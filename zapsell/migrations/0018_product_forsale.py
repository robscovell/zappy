# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zapsell', '0017_auto_20151214_0303'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='forsale',
            field=models.BooleanField(default=True),
        ),
    ]
