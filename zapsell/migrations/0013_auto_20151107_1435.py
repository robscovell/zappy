# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zapsell', '0012_auto_20151107_1434'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shippingaddress',
            old_name='seller',
            new_name='user',
        ),
    ]
