# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zapsell', '0002_auto_20150919_0712'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='rawJSON',
            field=models.CharField(max_length=65536, blank=True),
        ),
    ]
