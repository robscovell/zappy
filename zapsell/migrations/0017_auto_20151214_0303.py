# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zapsell', '0016_product_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='genre',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='condition',
            field=models.CharField(max_length=256, blank=True),
        ),
    ]
