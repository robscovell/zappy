# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zapsell', '0004_auto_20150919_0835'),
    ]

    operations = [
        migrations.RenameField(
            model_name='book',
            old_name='publisher_text',
            new_name='publisher',
        ),
    ]
