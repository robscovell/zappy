from django.contrib import admin
from zapsell.models import Book, Product, ShippingAddress

admin.site.register(Book)
admin.site.register(Product)
admin.site.register(ShippingAddress)

