from django.shortcuts import render
from django.forms import modelformset_factory
from zapsell.models import Product
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def listings(request):
    user = request.user
    products = Product.objects.filter(user = user)
#     ProductFormSet = modelformset_factory(Product, fields = ('book', 'price', 'condition', 'forsale'))
#     formset = ProductFormSet(queryset = Product.objects.filter(user = user))
    return render(request, 'listings.html', {"products": products,})
