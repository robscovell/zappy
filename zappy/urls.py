from django.conf.urls import include, url
from django.contrib import admin
from zapsell import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^listings/', views.listings),
]

